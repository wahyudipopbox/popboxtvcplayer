﻿using Microsoft.Win32;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms.Design;

namespace PopBoxTVCPlayer
{
  public class ScreensaverSettings
  {
    private static readonly string registryRoot = "HKEY_CURRENT_USER\\Software\\PopBox\\PopBoxTVCPlayer";

    public bool PlayAudio { get; set; }

    [Editor(typeof (FolderNameEditor), typeof (UITypeEditor))]
    public string MoviePath { get; set; }

    public void Save()
    {
      Registry.SetValue(ScreensaverSettings.registryRoot, "PlayAudio", (object) this.PlayAudio, RegistryValueKind.String);
      Registry.SetValue(ScreensaverSettings.registryRoot, "MoviePath", (object) this.MoviePath, RegistryValueKind.String);
    }

    public void Load()
    {
      string str1 = Registry.GetValue(ScreensaverSettings.registryRoot, "PlayAudio", (object) this.PlayAudio) as string;
      if (!string.IsNullOrEmpty(str1))
        this.PlayAudio = bool.Parse(str1);
      string str2 = Registry.GetValue(ScreensaverSettings.registryRoot, "MoviePath", (object) this.MoviePath) as string;
      if (string.IsNullOrEmpty(str2))
        return;
      this.MoviePath = str2;
    }
  }
}
