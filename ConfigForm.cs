﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace PopBoxTVCPlayer
{
  public class ConfigForm : Form
  {
    private IContainer components;
    private Button btnOK;
    private Button btnCancel;
    private PropertyGrid prpSettings;

    public ConfigForm(ScreensaverSettings settings)
    {
      this.InitializeComponent();
      this.prpSettings.SelectedObject = (object) settings;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.btnOK = new Button();
      this.btnCancel = new Button();
      this.prpSettings = new PropertyGrid();
      this.SuspendLayout();
      this.btnOK.DialogResult = DialogResult.OK;
      this.btnOK.Location = new Point(110, 227);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new Size(75, 23);
      this.btnOK.TabIndex = 0;
      this.btnOK.Text = "OK";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnCancel.DialogResult = DialogResult.Cancel;
      this.btnCancel.Location = new Point(192, 227);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new Size(75, 23);
      this.btnCancel.TabIndex = 1;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.prpSettings.Location = new Point(12, 12);
      this.prpSettings.Name = "prpSettings";
      this.prpSettings.Size = new Size(260, 209);
      this.prpSettings.TabIndex = 2;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(284, 262);
      this.Controls.Add((Control) this.prpSettings);
      this.Controls.Add((Control) this.btnCancel);
      this.Controls.Add((Control) this.btnOK);
      this.Name = "ConfigForm";
      this.Text = "ConfigForm";
      this.ResumeLayout(false);
    }
  }
}
