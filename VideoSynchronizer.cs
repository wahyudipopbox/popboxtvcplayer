﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;

namespace PopBoxTVCPlayer
{
  public class VideoSynchronizer
  {
    private static readonly VideoSynchronizer instance = new VideoSynchronizer();
    private List<ScreenSaverForm> screensavers = new List<ScreenSaverForm>();
    private List<BackgroundWorker> workers = new List<BackgroundWorker>();
    private List<string> movies = new List<string>();
    private List<string> history = new List<string>();
    private DateTime nextStart;
    private int nbReadyToPlay;

    public static VideoSynchronizer Instance
    {
      get
      {
        return VideoSynchronizer.instance;
      }
    }

    public string CurrentMovie
    {
      get
      {
        if (this.history.Count > 0)
          return this.history[this.history.Count - 1];
        return string.Empty;
      }
    }

    private VideoSynchronizer()
    {
    }

    public void RegisterScreensaver(ScreenSaverForm screensaver)
    {
      this.screensavers.Add(screensaver);
      screensaver.ReadyToPlay += new EventHandler(this.screensaver_ReadyToPlay);
      BackgroundWorker backgroundWorker = new BackgroundWorker();
      backgroundWorker.DoWork += new DoWorkEventHandler(this.bw_DoWork);
      this.workers.Add(backgroundWorker);
    }

    public void FindMovies(string path)
    {
      if (string.IsNullOrEmpty(path))
        return;
      this.movies.AddRange((IEnumerable<string>) Directory.GetFiles(path, "*.mov"));
      this.movies.AddRange((IEnumerable<string>) Directory.GetFiles(path, "*.avi"));
      this.movies.AddRange((IEnumerable<string>) Directory.GetFiles(path, "*.wmv"));
      this.movies.AddRange((IEnumerable<string>) Directory.GetFiles(path, "*.mp4"));
      if (this.movies.Count <= 0)
        return;
      this.history.Add(this.movies[0]);
    }

    private void bw_DoWork(object sender, DoWorkEventArgs e)
    {
      while (DateTime.Now.Ticks < this.nextStart.Ticks)
        Thread.Sleep(10);
      this.screensavers[(int) e.Argument].Start();
    }

    public void StartScreensavers()
    {
      if (this.movies.Count <= 0)
        return;
      this.history.Add(this.movies[new Random().Next(this.movies.Count)]);
      foreach (ScreenSaverForm screensaver in this.screensavers)
        screensaver.LoadVideo();
    }

    private void screensaver_ReadyToPlay(object sender, EventArgs e)
    {
      ++this.nbReadyToPlay;
      if (this.nbReadyToPlay != this.screensavers.Count)
        return;
      this.nextStart = DateTime.Now.AddSeconds(1.0);
      for (int index = 0; index < this.workers.Count; ++index)
        this.workers[index].RunWorkerAsync((object) index);
      this.nbReadyToPlay = 0;
    }
  }
}
