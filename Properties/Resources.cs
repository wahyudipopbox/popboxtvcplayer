﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace PopBoxTVCPlayer.Properties
{
  [DebuggerNonUserCode]
  [CompilerGenerated]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (PopBoxTVCPlayer.Properties.Resources.resourceMan == null)
          PopBoxTVCPlayer.Properties.Resources.resourceMan = new ResourceManager("PopBoxTVCPlayer.Properties.Resources", typeof (PopBoxTVCPlayer.Properties.Resources).Assembly);
        return PopBoxTVCPlayer.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return PopBoxTVCPlayer.Properties.Resources.resourceCulture;
      }
      set
      {
        PopBoxTVCPlayer.Properties.Resources.resourceCulture = value;
      }
    }

    internal Resources()
    {
    }
  }
}
