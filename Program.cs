﻿using System;
using System.Windows.Forms;

namespace PopBoxTVCPlayer
{
  internal static class Program
  {
    [STAThread]
    private static void Main(string[] args)
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      if (args.Length > 0)
      {
        string str = args[0].ToLower().Trim();
        string s = (string) null;
        if (str.Length > 2)
        {
          s = str.Substring(3).Trim();
          str = str.Substring(0, 2);
        }
        else if (args.Length > 1)
          s = args[1];
        if (str == "/c")
          Program.LoadSettings();
        else if (str == "/p")
        {
          if (s == null)
          {
            int num1 = (int) MessageBox.Show("Sorry, but the expected window handle was not provided.", "ScreenSaver", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
          }
          else
          {
            ScreensaverSettings settings = new ScreensaverSettings();
            settings.Load();
            VideoSynchronizer.Instance.FindMovies(settings.MoviePath);
            Application.Run((Form) new ScreenSaverForm(new IntPtr(long.Parse(s)), settings));
          }
        }
        else if (str == "/s")
        {
          Program.ShowScreenSaver();
          Application.Run();
        }
        else
        {
          int num2 = (int) MessageBox.Show("Sorry, but the command line argument \"" + str + "\" is not valid.", "ScreenSaver", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
      }
      else
        Program.LoadSettings();
    }

    private static void ShowScreenSaver()
    {
      ScreensaverSettings settings = new ScreensaverSettings();
      settings.Load();
      VideoSynchronizer.Instance.FindMovies(settings.MoviePath);
      int length = Screen.AllScreens.Length;
      for (int screenIndex = 0; screenIndex < length; ++screenIndex)
      {
        ScreenSaverForm screensaver = new ScreenSaverForm(screenIndex, settings);
        VideoSynchronizer.Instance.RegisterScreensaver(screensaver);
        screensaver.Show();
      }
      VideoSynchronizer.Instance.StartScreensavers();
    }

    private static void LoadSettings()
    {
      ScreensaverSettings settings = new ScreensaverSettings();
      settings.Load();
      if (new ConfigForm(settings).ShowDialog() != DialogResult.OK)
        return;
      settings.Save();
    }
  }
}
